package com.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TwilioConfig {

	@Value("${twilio.account_sid}")
	private String accountSid;

	@Value("${twilio.auth_token}")
	private String authToken;


    public String getAccountSid() {
        return accountSid;
    }

    public String getAuthToken() {
        return authToken;
    }
}
