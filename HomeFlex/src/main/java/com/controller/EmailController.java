package com.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.dao.EmailService;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "http://localhost:4200")
public class EmailController {

    @Autowired
    private EmailService emailService;

    public Map<String, String> emailOtpMap = new HashMap<>();

    @PostMapping("/sendMail")
    public ResponseEntity<Object> sendEmail(@RequestBody Map<String, Object> requestBody) {
        String toEmail = (String) requestBody.get("email");
        String subject = (String) requestBody.get("subject");
        String body = (String) requestBody.get("content");

        try {
            emailService.sendEmail(toEmail, subject, body);
            return ResponseEntity.ok(true);
        } catch (Throwable t) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Internal Server Error: " + t.getLocalizedMessage());
        }
    }

    @PostMapping("/sendOtp")
    public ResponseEntity<Object> sendOtp(@RequestBody Map<String, String> request) {
        String toEmail = request.get("email");
        String subject = "Homeflex - Email Verification";
        String bodyContent = "Your Email verification OTP for Homeflex registration is: ";

        // Generate a random 6-digit OTP
        String body = String.format("%06d", new Random().nextInt(1000000));

        // Store the OTP in memory
        emailOtpMap.put(toEmail, body);

        try {
            emailService.sendEmail(toEmail, subject, bodyContent + body);
            return ResponseEntity.ok(true);
        } catch (Throwable t) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Internal Server Error: " + t.getLocalizedMessage());
        }
    }

    @GetMapping("/fetchOtp/{email}/{givenOtp}")
    public ResponseEntity<Boolean> fetchOtp(@PathVariable String email, @PathVariable String givenOtp) {
        String storedOtp = emailOtpMap.get(email);

        if (storedOtp != null && storedOtp.equals(givenOtp)) {
            return ResponseEntity.ok(true);
        } else {
            return ResponseEntity.ok(false);
        }
    }
}
