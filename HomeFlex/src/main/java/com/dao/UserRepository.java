package com.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.model.User;

public interface UserRepository extends JpaRepository<User, Long> {

    @Query("from User where userEmail=:emailId and userPassword=:password")
    List<User> userLogin(@Param("emailId") String emailId, @Param("password") String password);

    Optional<User> findByuserEmail(String email);
}
