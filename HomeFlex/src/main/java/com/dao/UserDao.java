package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.model.User;

@Service
public class UserDao {

    @Autowired
    private UserRepository userRepo;

    public List<User> getAllUsers() {
        return userRepo.findAll();
    }

    public User registerUser(User user) {
        return userRepo.save(user);
    }

    public User userLogin(String emailId, String password) {
        List<User> users = userRepo.userLogin(emailId, password);

        if (users.size() == 1) {
            User user = users.get(0);
            if (passwordMatches(password, user.getUserPassword())) {
                return user;
            }
        }
        return null;
    }

    private boolean passwordMatches(String rawPassword, String encodedPassword) {
        return new BCryptPasswordEncoder().matches(rawPassword, encodedPassword);
    }

    public void deleteUserById(Long userId) {
        userRepo.deleteById(userId);
    }

    public User updateUser(User user) {
        return userRepo.save(user);
    }

    public void deleteAllUsers() {
        userRepo.deleteAll();
    }

    public User changePassword(String email, String newPassword) {
        User user = userRepo.findByuserEmail(email).orElse(null);

        if (user != null) {
            String encryptedPassword = new BCryptPasswordEncoder().encode(newPassword);
            user.setUserPassword(encryptedPassword);
            return userRepo.save(user);
        } else {
            return null; // Handle case where the user is not found
        }
    }

    public User getUserById(Long userId) {
        return userRepo.findById(userId).orElse(null);
    }
}
